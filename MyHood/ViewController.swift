//
//  ViewController.swift
//  MyHood
//
//  Created by Juan Ramirez on 5/9/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.estimatedRowHeight = 87 //For allowing the cell to change height flexibly

        DataService.instance.loadPosts()    // Load posts
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onPostsLoaded:", name: "postsLoaded", object: nil)
        
        
        tableView.reloadData()  //Anytime you modify a post or add one you MUST reload tableView
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //To populate the data that you will show
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //Loads all the posts from memory
        //DataService.instance.loadPosts()
        
        let post = DataService.instance.loadedPosts[indexPath.row]
        
        //This will re-use a cell and must update data
        if let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell {
            
            //Updating data of cell
            cell.configureCell(post)
            return cell
        } else {    //Enters if it doesnt give you a reusable cell and you configure your own
            
            var cell = PostCell()
            cell.configureCell(post)
            
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 87.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.loadedPosts.count
    }
    
//    //When you want to load a view when you select a row
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//    }

    func onPostsLoaded(notification: AnyObject) {
        tableView.reloadData()
    }

}

