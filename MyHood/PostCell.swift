//
//  PostCell.swift
//  MyHood
//
//  Created by Juan Ramirez on 5/9/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        postImage.layer.cornerRadius = postImage.frame.width / 2
        postImage.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(post: Post) {
        postImage.image = DataService.instance.imageForPath(post.imagePath)
        titleLabel.text = post.title
        descLabel.text = post.postDescription
        
    }

}
