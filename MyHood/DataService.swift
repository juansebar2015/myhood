//
//  DataService.swift
//  MyHood
//
//  Created by Juan Ramirez on 5/10/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import UIKit

//Singleton- One instance assesible from everywhere

class DataService {
    
    static let instance = DataService()     //Has only one instance
    
    let KEY_POSTS = "posts"
    
    private var _loadedPosts = [Post]()
    
    var loadedPosts: [Post] {
        return _loadedPosts
    }
    
    
    func savePosts() {
        //Turning array to data
        let postsData = NSKeyedArchiver.archivedDataWithRootObject(_loadedPosts)
        NSUserDefaults.standardUserDefaults().setObject(postsData, forKey: KEY_POSTS)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadPosts() {
        
        // Get the Data
        if let postData = NSUserDefaults.standardUserDefaults().objectForKey(KEY_POSTS) as? NSData {
            
            // Turn Data into Array
            if let postsArray = NSKeyedUnarchiver.unarchiveObjectWithData(postData) as? [Post] {
                _loadedPosts = postsArray
            }
        }
        
        // Notify anyone who cares that the notification has been posted
        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "postsLoaded", object: nil))
    }
    
    func saveImageAndCreatePath(image: UIImage) -> String{
        let imageData = UIImagePNGRepresentation(image)
        let imagePath = "image\(NSDate.timeIntervalSinceReferenceDate()).png"
        let fullPath = documentPathForFileName(imagePath)
        
        imageData?.writeToFile(fullPath, atomically: true)
        
        return imagePath
    }
    
    func imageForPath(path: String) -> UIImage? {
        let fullPath = documentPathForFileName(path)
        let image = UIImage(named: fullPath)
        
        return image
    }
    
    func addPost(post: Post) {
        
        _loadedPosts.append(post)
        
        savePosts()     // Save in memory
        loadPosts()      // Refresh the list and show post just loaded
    }
    
    func documentPathForFileName(name: String) -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let fullPath = paths[0] as NSString //get first element out of it
        
        return fullPath.stringByAppendingPathComponent(name)
        
    }
}