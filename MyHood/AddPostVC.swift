//
//  AddPostVC.swift
//  MyHood
//
//  Created by Juan Ramirez on 5/10/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
                                    //Must have both PickerDelegate and NavControlDelegate to select picture
class AddPostVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Outlets
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!   // ! -> guarantees that there will be a value in it
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postImage.layer.cornerRadius = postImage.frame.width / 2.0
        postImage.clipsToBounds = true      //Done to be able to see the circle
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
    }
    
    //Actions
    @IBAction func makePostButtonPressed(sender: AnyObject) {
        
        // Making sure all the parameters exists before making post
        if let title = titleTextField.text, let desc = descriptionTextField.text, let img = postImage.image {
         
            let imgPath = DataService.instance.saveImageAndCreatePath(img)
            
            let newPost = Post(imagePath: imgPath, title: title, description: desc)
            
            
            DataService.instance.addPost(newPost)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addPicButtonPressed(sender: UIButton) {
        
        //Hide the title
        sender.setTitle("", forState: .Normal)
        
        // Show the Image Picker View Controller
        presentViewController(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)    //Hide imagePicker
        postImage.image = image     // Image returned after user finishes picking a picture
    }
}
